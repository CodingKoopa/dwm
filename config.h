/* See LICENSE file for copyright and license details. */

// Necessary for volume keys.
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "family=SF Pro Display:style=Regular:pixelsize=16" };
static const char dmenufont[]       = "monospace:size=10";
// Catppucin Frappe
static const char col_text[]        = "#c6d0f5";
static const char col_subtext0[]    = "#a5adce";
static const char col_subtext1[]    = "#b5bfe2";
static const char col_surface0[]    = "#414559";
static const char col_overlay0[]    = "#737994";
static const char col_overlay1[]    = "#838ba7";
static const char col_overlay2[]    = "#737994";
static const char col_mantle[]      = "#292c3c";
static const char col_base[]        = "#303446";
static const char col_primary[]     = "#ca9ee6"; // e.g. rosewater

static const char col_test[]        = "#fc03a5";
// We take the position that no text here will use the Text color.
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	/* Used for tags and status bar.
	   Border is also used for inactive windows.
	   fg: This is just normal content text.
	   bg: The Base color is considered to not be applicable for all of this.
	   For the background of the tags and status bar, we start instead with the Mantle.
	   border: The darkest split color is used.
	 */
	// 
	[SchemeNorm] = { col_text, col_mantle, col_overlay2 },
	/* Used for selected tags *and* selected tabs.
	   fg: This is whatever the SchemeNorm background is.
	   bg: Primary color, for nicely accenting the active stuff.
	   border: (disabled)
	*/
	[SchemeSel]  = { col_mantle, col_primary,  col_primary  },
	/* Colors for when the tabs are "active" in the sense that we are in monocle mode.
	   This has nothing to do with whether a window has focus/is selected.
	   If the bottom border is enabled, then the fg will also be used as a bottom border
	   for the whole bar, regardless of layout.
	   fg: Inactive text color.
	   bg: Next surface color.
	   border: (disabled)
	 */
	[SchemeTabActive]  = { col_overlay1, col_surface0,  col_test },
	/* Colors for when the tabs are inactive
	   fg: Normal text color.
	   bg: Next surface color.
	   border: (disabled)
	 */
	[SchemeTabInactive]  = { col_text, col_surface0,  col_test }
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	// Tag 1: Firefox and/or game.
	{ "firefox",        NULL,       NULL,       1 << 0,       0,           -1 },
	// Tag 2: Discord. Set as floating so that it can be pulled up at any time.
	// { "discord",        NULL,       NULL,       1 << 1,       1,           1 },
	{ "discord",        NULL,       NULL,       1 << 1,       1,           1 },
	// Tag 3: Terminal.
	{ "st",             NULL,       NULL,       1 << 2,       0,           -1 },
	{ "konsole",             NULL,       NULL,       1 << 2,       0,           -1 },
	// Tag 4: Code editor.
	{ "code-oss",       NULL,       NULL,       1 << 3,       0,           -1 },
	// Tag 5: Launcher
	{ "zoom ",       NULL,       NULL,       1 << 4,       0,           -1 },
	// Tag 6: Event.
	{ "Gimp",           NULL,       NULL,       1 << 5,       0,           -1 },
	// Tag 7: Google Chrome.
	{ "Google-chrome",  NULL,	      NULL,       1 << 6,       0,           0 },
	// Tag 8: Password manager.
	{ "KeePassXC",		  NULL,       NULL,       1 << 7,       1,           1 },
	// Tag 8: Break timer.
	// Workrave presents itself as floating.
	{ "Workrave",		  NULL,       NULL,         1 << 7,       0,           -1 },

	// Tag 6: Steam
	{ "Steam", NULL,       NULL,         1 << 4,       1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

/* Bartabgroups properties */
#define BARTAB_BORDERS 0       // 0 = off, 1 = on
#define BARTAB_BOTTOMBORDER 0  // 0 = off, 1 = on
#define BARTAB_TAGSINDICATOR 0 // 0 = off, 1 = on if >1 client/view tag, 2 = always on
#define BARTAB_TAGSPX 5        // # pixels for tag grid boxes
#define BARTAB_TAGSROWS 3      // # rows in tag grid (9 tags, e.g. 3x3)
static void (*bartabmonfns[])(Monitor *) = { monocle /* , customlayoutfn */ };
static void (*bartabfloatfns[])(Monitor *) = { NULL /* , customlayoutfn */ };

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[M]",      monocle },
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      toggleview,   {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      view,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_overlay2, "-nf", col_text, "-sb", col_primary, "-sf", col_base, NULL };
static const char *termcmd[]  = { "konsole", NULL };

static const char *upvol[]   = { "/usr/bin/pactl", "set-sink-volume", "@DEFAULT_SINK@", "+5%",     NULL };
static const char *downvol[] = { "/usr/bin/pactl", "set-sink-volume", "@DEFAULT_SINK@", "-5%",     NULL };
static const char *mutevol[] = { "/usr/bin/pactl", "set-sink-mute",   "@DEFAULT_SINK@", "toggle",  NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */

	// Toggling Bar
	{ MODKEY,                       XK_b,      togglebar,      {0} },

	// Switching Layouts
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[0]} },

	// Launching Programs
	{ MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },

	// Navigating Clients (vim style)
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	// Navigating Clients (arrow keys)
	{ MODKEY,                       XK_Down,   focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_Up,     focusstack,     {.i = -1 } },

	{ MODKEY,                       XK_equal,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_minus,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Left,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_Right,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Return, zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	// Doesn't support more than one monitor but that's okay.
	{ MODKEY,                       XK_d, focusmon,        {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_d,  tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_q,                      2)
	TAGKEYS(                        XK_w,                      3)
	TAGKEYS(                        XK_a,                      4)
	TAGKEYS(                        XK_s,                      5)
	TAGKEYS(                        XK_z,                      6)
	TAGKEYS(                        XK_x,                      7)

	// Media Keys
	{ 0,                       XF86XK_AudioLowerVolume, spawn, {.v = downvol } },
	{ 0,                       XF86XK_AudioMute, spawn, {.v = mutevol } },
	{ 0,                       XF86XK_AudioRaiseVolume, spawn, {.v = upvol   } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        toggleview,     {0} },
	{ ClkTagBar,            0,              Button3,        view,           {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};
